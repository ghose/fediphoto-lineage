CHANGELOG FOR RELEASE 3.6

First release of this fork!

- Changed logos, colours and package names.
- Updated some libraries versions.
