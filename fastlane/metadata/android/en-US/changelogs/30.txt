CHANGELOG FOR RELEASE 3.9

Small improvements and bugfixes in this release:

- Fixed app name and updated screenshots for F-Droid (#15 and #17)
- Info button in menu now points to our page (#24)
- Location is no more posted if GPS location format is blank (#30)
- Solved some crashy situations
- Other minor bugfixes (#18 and others not even listed)
