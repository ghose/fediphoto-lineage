# FediPhoto-Lineage

Post a photo quickly to the Fediverse...part II

[https://silkevicious.codeberg.page/fediphoto-lineage.html](https://silkevicious.codeberg.page/fediphoto-lineage.html)

*Open, Click, Shared!*

This is a continuation of the [FediPhoto](https://github.com/pla1/FediPhoto) project by **PLA**, who left us in January 2021. We'll do our best to honour his memory and his work.

## Install

You have many ways to download this app:

<table>

<tr>
<th align="center" valign="center" width="33%">
<h3>Codeberg Releases</h3>
</th>
<th align="center" valign="center" width="33%">
<h3>Silkevicious ApkRepo</h3>
</th>
<th align="center" valign="center" width="33%">
<h3>F-Droid</h3>
</th>
</tr>

<tr>
<td align="center" valign="center">
<p>
<a href="https://codeberg.org/silkevicious/fediphoto-lineage/releases"><img src="https://silkevicious.codeberg.page/images/codeberg.png" height="80" alt="Codeberg badge"></a>
</p>
</td>
<td align="center" valign="center">
<p>
<a href="https://codeberg.org/silkevicious/apkrepo"><img src="https://silkevicious.codeberg.page/images/apkrepo.png" height="80" alt="ApkRepo badge"></a>
</p>
</td>
<td align="center" valign="center">
<p>
<a href="https://f-droid.org/packages/com.fediphoto.lineage/"><img src="https://silkevicious.codeberg.page/images/fdroid.png" height="80" alt="F-Droid badge"></a>
</p>
</td>
</tr>

</table>


For the original FediPhoto application see [HERE](https://github.com/pla1/FediPhoto#install)
     
## How-To

* connect **FediPhoto-Lineage** client with your _fediverse_ account (mastodon, pleroma, friendica and pixelfed).
     * multiple accounts are supported.
* you'll be requested to concede this [permissions](https://codeberg.org/silkevicious/fediphoto-lineage/src/branch/master/PRIVACY.md)
* configure status settings-
     * multiple status configurations are supported.
* tap on main screen to open camera, take picture and click on check mark icon(¹) (bottom right)
* it's done!! FediPhoto-Lineage will publish your status and will receive a confirmation message afterwards

(¹) **WARNING** : this behaviour happens just if using the default Android Camera app.
When using another camera app like _OpenCamera_ or _SimpleCamera_ you aren't asked for confirmation,
hence the photo is uploaded automatically right after the shoot. Beware what you frame then!
This procedure will change in the future (see related issue #42)

## Screenshots
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3.png" width="150">
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="150">

## Changelog

[FediPhoto-Lineage Changelog](https://codeberg.org/silkevicious/fediphoto-lineage/src/branch/master/CHANGELOG.md)

## Privacy

[FediPhoto-Lineage Privacy Policy](https://codeberg.org/silkevicious/fediphoto-lineage/src/branch/master/PRIVACY.md)

**FediPhoto-Lineage** does NOT collect any personal data and/or information. It works locally on your device and information about your account is not shared or stored.

## Original Author

* [pla1](https://github.com/pla1)

## Developers

* [Sylke Vicious](https://codeberg.org/silkevicious)
* [කසුන්](https://codeberg.org/0xd9a)

## Bug Reports & Contributions

Please **open** a [issue](https://codeberg.org/silkevicious/fediphoto-lineage/issues) with detailed information about the problem.

If you're not comfortable to fill an issue or don't have an account here on Codeberg, reach out us at our [fediverse account](https://libranet.de/profile/fediphoto_lineage).

**Contribute** making feature requests and **code** contributions.

Contact translators for translation issues.

## Localization

Translate **FediPhoto-Lineage** _UI_

* **Fork** this repository
* Add *value-XX* folder to **app/src/main/res/** directory
	* where **XX** is Android's language **ISO-639-1** code (e.g. de, es, fr, it, ...)
* Copy _strings.xml_ from _values_ folder into this new created folder for your language
* Translate it and make a PR to this repository
 
Translate **FediPhoto-Lineage** _F-Droid Descriptions_

* **Fork** this repository
* Add *XX* folder to **fastlane/metadata/android/** directory
	* where **XX** is Android's language **ISO-639-1** code (e.g. de, es, fr, it, ...)
* Copy _full_description.txt_ and _short_description.txt_ from en-US folder into this new created folder for your language
* Translate it and make a PR to this repository

We planned however to setup a Weblate instance in the future. See progress in the related [issue](#23).

### Contributors (A-Z)

* Catalan - [spla](https://github.com/splacat)
* Euskera - [Izaro](https://pleroma.libretux.com/users/izaro)
* Galician - [xmgz](https://github.com/xmgz) 
* Italian - [Sylke Vicious](https://codeberg.org/silkevicious)
* Spanish - [xmgz](https://github.com/xmgz)