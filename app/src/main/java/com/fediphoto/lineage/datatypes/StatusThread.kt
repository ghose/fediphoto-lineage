package com.fediphoto.lineage.datatypes

data class StatusThread(
    val accountId: Int,
    val templateId: Int,
    var statusId: String,
    var statusDate: String
)
