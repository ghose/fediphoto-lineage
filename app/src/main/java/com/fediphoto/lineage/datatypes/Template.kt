package com.fediphoto.lineage.datatypes

import com.fediphoto.lineage.Helper
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility

data class Template(
    val id: Int,
    val name: String,
    val text: String,
    val visibility: Visibility,
    val threading: Threading = Threading.DAILY,
    val date: Boolean,
    val dateFormat: String,
    val location: Boolean,
    val locationFormat: String = Helper.DEFAULT_LOCATION_FORMAT
)
