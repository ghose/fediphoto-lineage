package com.fediphoto.lineage.datatypes.enums

enum class Visibility {
    PUBLIC,
    UNLISTED,
    PRIVATE,
    DIRECT;
    val urlValue = name.lowercase()
}
