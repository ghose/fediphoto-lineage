package com.fediphoto.lineage

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.fediphoto.lineage.databinding.ActivityAccountBinding
import com.fediphoto.lineage.datatypes.LocalAccount

class AccountActivity : AppCompatActivity() {
    private val context: Context = this

    private lateinit var viewBinding: ActivityAccountBinding

    private lateinit var account: LocalAccount
    private lateinit var prefs: Prefs
    private lateinit var database: Database

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityAccountBinding.inflate(layoutInflater)
        prefs = Prefs(context)
        database = Database(context)

        account = Database(context).getAccountById(intent.getIntExtra(SELECTED_ACCOUNT_ID, -1))!!
        viewBinding.textViewInstance.text = account.instance
        viewBinding.textViewUserUrl.text = account.userUrl

        viewBinding.makeActive.isEnabled = prefs.activeAccountId != account.id
        viewBinding.makeActive.setOnClickListener {
            Prefs(this).activeAccountId = account.id
            it.isEnabled = false
        }

        viewBinding.removeAccount.setOnClickListener {
            Log.i(TAG, "Remove account.")
            database.removeAccount(account.id) {
                Toast.makeText(context, R.string.account_removed_account_0_is_active, Toast.LENGTH_LONG).show()
                database.getAccounts().let { accounts ->
                    if (accounts.isEmpty()) {
                        prefs.activeAccountId = -1
                        setResult(MainActivity.REQUEST_ACCOUNT_RETURN, intent)
                    } else {
                        if (prefs.activeAccountId == account.id) prefs.activeAccountId = accounts[0].id
                        setResult(RESULT_OK, intent)
                    }
                }
                finish()
            }
        }
        setContentView(viewBinding.root)
    }

    companion object {
        const val SELECTED_ACCOUNT_ID = "selected_account"
        private val TAG = this::class.java.canonicalName
    }
}
