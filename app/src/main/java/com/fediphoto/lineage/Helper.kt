package com.fediphoto.lineage

import com.fediphoto.lineage.datatypes.Location
import com.fediphoto.lineage.datatypes.Template
import com.fediphoto.lineage.datatypes.enums.Threading
import com.fediphoto.lineage.datatypes.enums.Visibility

object Helper {
    const val REQUESTED_SCOPES = "read write"
    const val RETURN_URL = "fediphoto://fediphotoreturn"
    const val WEBSITE = "https://silkevicious.codeberg.page/fediphoto-lineage.html"

    const val KEY_URL = "url"

    private const val PATTERN_LOCATION_LATITUDE = "%lt"
    private const val PATTERN_LOCATION_LONGITUDE = "%ln"
    const val DEFAULT_LOCATION_FORMAT: String = "https://www.openstreetmap.org/#map=19/$PATTERN_LOCATION_LATITUDE/$PATTERN_LOCATION_LONGITUDE"
    const val DEFAULT_DATE_FORMAT = "EEEE MMMM dd, yyyy hh:mm:ss a z"
    val eiffelTowerLocation = Location(48.85827F, 2.29443F)
}
