package com.fediphoto.lineage

import android.util.Log
import com.fediphoto.lineage.datatypes.FediAccount
import okhttp3.*
import okio.IOException
import org.json.JSONObject

class API {
    companion object {
        private const val TAG = "api"
    }

    fun createApp(instanceDomain: String, onSuccess: (redirectUri: String, clientId: String, clientSecret: String) -> Unit) {
        Log.i(TAG, "createApp: Begin")
        val client = OkHttpClient()

        val httpUrl: HttpUrl = HttpUrl.Builder()
            .scheme("https")
            .host(instanceDomain)
            .addPathSegment("api")
            .addPathSegment("v1")
            .addPathSegment("apps")
            .build()

        val formBody = FormBody.Builder()
            .add("client_name", "FediPhoto-Lineage for Android")
            .add("redirect_uris", Helper.RETURN_URL)
            .add("scopes", Helper.REQUESTED_SCOPES)
            .add("website", Helper.WEBSITE)
            .build()

        val request = Request.Builder()
            .url(httpUrl)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: java.io.IOException) = e.printStackTrace()

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (it.isSuccessful) {
                        Log.i(TAG, "createApp: Response received")
                        try {
                            it.body?.string().toString().let { responseBody ->
                                val redirectUri: String = JSONObject(responseBody).getString("redirect_uri")
                                val clientId: String = JSONObject(responseBody).getString("client_id")
                                val clientSecret: String = JSONObject(responseBody).getString("client_secret")

                                if (redirectUri.isNotBlank() && clientId.isNotBlank() && clientSecret.isNotBlank()) {
                                    Log.i(TAG, "createApp: Calling onSuccess")
                                    onSuccess.invoke(redirectUri, clientId, clientSecret)
                                } else Log.e(TAG, "createApp: Expected values not found")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else throw IOException("Unexpected code $it")
                }
            }
        })
    }

    fun getAccessToken(
        instanceDomain: String,
        clientId: String,
        clientSecret: String,
        redirectUri: String,
        authorizationCode: String,
        onSuccess: (accessToken: String) -> Unit
    ) {
        Log.i(TAG, "getAccessToken: Begin")
        val client = OkHttpClient()

        val httpUrl: HttpUrl = HttpUrl.Builder()
            .scheme("https")
            .host(instanceDomain)
            .addPathSegment("oauth")
            .addPathSegment("token")
            .build()

        val formBody = FormBody.Builder()
            .add("client_id", clientId)
            .add("client_secret", clientSecret)
            .add("grant_type", "authorization_code")
            .add("redirect_uri", redirectUri)
            .add("code", authorizationCode)
            .add("scope", Helper.REQUESTED_SCOPES)
            .build()

        val request = Request.Builder()
            .url(httpUrl)
            .post(formBody)
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: java.io.IOException) = e.printStackTrace()

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (it.isSuccessful) {
                        Log.i(TAG, "getAccessToken: Response received")
                        try {
                            it.body?.string().toString().let { responseBody ->
                                val accessToken = JSONObject(responseBody).getString("access_token")
                                if (accessToken.isNotBlank()) {
                                    Log.i(TAG, "getAccessToken: Calling onSuccess")
                                    onSuccess.invoke(accessToken)
                                } else Log.e(TAG, "getAccessToken: Expected values not found")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else throw IOException("Unexpected code $it")
                }
            }
        })
    }

    fun verifyToken(instanceDomain: String, accessToken: String, onSuccess: (instanceDomain: String, account: FediAccount) -> Unit) {
        Log.i(TAG, "verifyToken: Begin")
        val client = OkHttpClient()

        val httpUrl: HttpUrl = HttpUrl.Builder()
            .scheme("https")
            .host(instanceDomain)
            .addPathSegment("api")
            .addPathSegment("v1")
            .addPathSegment("accounts")
            .addPathSegment("verify_credentials")
            .build()

        val request = Request.Builder()
            .url(httpUrl)
            .addHeader("Authorization", "Bearer $accessToken")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: java.io.IOException) = e.printStackTrace()

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (it.isSuccessful) {
                        Log.i(TAG, "verifyToken: Response received")
                        try {
                            it.body?.string().toString().let { responseBody ->
                                val id = JSONObject(responseBody).getString("id")
                                val username = JSONObject(responseBody).getString("username")
                                val acct = JSONObject(responseBody).getString("acct")
                                val displayName = JSONObject(responseBody).getString("display_name")
                                val url = JSONObject(responseBody).getString("url")
                                val avatar = JSONObject(responseBody).getString("avatar")

                                if (id.isNotBlank() && username.isNotBlank() && acct.isNotBlank() && displayName.isNotBlank() && url.isNotBlank()) {
                                    Log.i(TAG, "verifyToken: Calling onSuccess")
                                    onSuccess.invoke(instanceDomain, FediAccount(id, username, acct, displayName, url, avatar))
                                } else Log.e(TAG, "verifyToken: Expected values not found")
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else throw IOException("Unexpected code $it")
                }
            }
        })
    }
}
